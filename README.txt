
README.txt
==========

********************************************************************
This is i18n menu admin 7.x, and will work with Drupal 7.x
********************************************************************
WARNING: DO READ THE INSTALL FILE AND the ON-LINE HANDBOOK
********************************************************************

This is an enhancement module for i18n menu.
The module i18n admin menu allow users to see items in a more organized
menu, separated by language.
Currently the module i18n_menu doesn't have a proper treatment
for it. I created this module with a dependency of i18n_menu, it
aims to improve the menus in the administrative interface.

Additional Support
=================
For support, please create a support request for this module's project:
http://drupal.org/project/i18n_menu_admin

Support questions by email to the module maintainer will be simply ignored.
Use the issue tracker.

Now if you want professional (paid) support the module maintainer
may be available occasionally.
Drop me a message to check availability and hourly rates,
http://taller.net.br


====================================================================
Helal Ferrari Cabral, drupal at reyero dot net, http://www.taller.net.br/
